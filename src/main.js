import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './router/beforeEnter';
import { createPinia } from 'pinia';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import locale from 'element-plus/lib/locale/lang/zh-cn';

import './styles/public.css';
import "@/styles/tailwindcss.css";
import ElIcons from './components/elIcons';

const app = createApp(App);
app.component('el-icons', ElIcons);

app.use(router)
  .use(createPinia())
  .use(ElementPlus, { locale })
  .mount('#app');
