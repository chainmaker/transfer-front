const TASKID = 'taskid';
export function setTaskId (ID) {
  localStorage.setItem(TASKID, ID);
}
export function getTaskId () {
  return localStorage.getItem(TASKID);
}
export function clearTaskId () {
  localStorage.removeItem(TASKID);
}