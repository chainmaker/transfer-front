import { ElImageViewer } from 'element-plus';
import { isImg, isObj } from '@/utils/util'
import { createVNode, render } from 'vue';


function imagePreview (options) {
  const container = document.createElement('div', { id: 'preview-img' });
  const vm = createVNode(ElImageViewer, {
    urlList: options.urlList,
    teleported: true,
    onClose () {
      render(null, container);
    }
  });
  render(vm, container);
  document.body.appendChild(vm.el);
}

export function previewFile (url) {
  if (isObj(url)) {
    return imagePreview(url);
  } else if (isImg(url)) {
    return imagePreview({
      urlList: [url]
    });
  } else {
    window.open(url)
  }
}

export function officeFilePreview (url) {
  let arr = url.split('.');
  let type = arr[arr.length - 1];
  if (type === 'doc' || type === 'docx' || type === 'xls' || type === 'xlsx' || type === 'ppt' || type === 'pptx') {
    url = 'https://view.officeapps.live.com/op/view.aspx?src=' + url
  }
  window.open(url)
}