import axios from 'axios';
import { useUserStore } from '@/store';
import { ElMessage, ElMessageBox } from 'element-plus';
import { formatParameter } from './util';


const ERROR_MSG = '接口错误，请联系管理员';
let baseUrl;
if (process.env.NODE_ENV == 'development') { // 测试环境配置
  baseUrl = window.VUE_APP_API_HOST + '/transfer'; //process.env.VUE_APP_API_HOST
} else { // 生产环境配置
  // baseUrl = 'http://172.16.11.47:12091/transfer';
  // baseUrl = 'http://192.168.0.173:12091/transfer';
  baseUrl = window.VUE_APP_API_HOST + '/transfer';
}

// create an axios instance
const service = axios.create({
  baseURL: baseUrl,
  timeout: 60000,
  // headers: {
  //   'Content-Type': 'application/json;charset=utf-8'
  // },
  // withCredentials: true // 当跨域请求时，是否携带cookie。项目中使用了proxy解决跨域，不需要这个属性
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    const params = {
      ...(config.params || config.data || {})
    };
    


    if (config.method === 'get' || config.method === 'delete') {
      config.params = params;
      delete config.data;
    } else {
      config.data = params;
      delete config.params;
    }
    // params;
    formatParameter(params);
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  
  (response) => {
    const res = response.data;
    if (res.code === '9100') {
      console.log(res);
      ElMessageBox.alert(
        res.retdetail || res.msg || ERROR_MSG,
        '登录过期',
        {
          type: 'warning',
          confirmButtonText: '重新登录',
          callback: () => {
            useUserStore.$reset();
            window.location.reload();
          },
        });
      return Promise.reject(res);
    } else if (res.code !== 200) {
      ElMessage.error(res.retdetail || res.msg || ERROR_MSG);
      return Promise.reject(res);
    }

    return Promise.resolve(res);
  },
  (error) => {
    // debugger;
    // var msg = JSON.stringify(JSON.parse(error.response.data));
    // console.log(msg);
    if (error && error.response && error.response.status >= 400) {
      if (error.response.status === 500) {
        ElMessage.error(error.response.data.msg || ERROR_MSG);
      } else if (error.response.status === 429) {
        ElMessage.error('操作太频繁，请稍后重试！');
      } else {
        ElMessage.error(error.response.data.msg || ERROR_MSG);
      }
    } else {
      ElMessage.error('网络开小差了');
    }
  }
);

function requestCreater (url, method = 'get',option,config = {}) {
  return data => {
    let parmasStr = '';
    try {
      if (config.isParmas) {
        parmasStr += '?';
        Object.keys(data).forEach((k) => {
          if (typeof data[k] != 'object' && typeof data[k] != 'undefined') {
            parmasStr = parmasStr + k + '=' + data[k] + "&";
            delete data[k];
          }
        });

        parmasStr = parmasStr.slice(0, -1);
       
        // url = url + '?' + parmasStr;
      }
    }
    catch (e) {
      console.log(e);
    }
    return service({
      url: url + parmasStr,
      data,
      method,
      ...option,
    });
  };
}


function request (url,option) {
  let fn = requestCreater(url, 'get',option);
  fn.GET = fn.get = requestCreater(url, 'get',option);
  fn.POST = fn.post = requestCreater(url, 'post',option);
  fn.PUT = fn.put = requestCreater(url, 'put',option);
  fn.DELETE = fn.delete = requestCreater(url, 'delete',option);
  return fn;
}
function requestByIdCreater (url, method) {
  return function (id) {
    return requestCreater(url + id, method)();
  };
}

export {
  request,
  requestCreater,
  requestByIdCreater,
  service
};

export default request;
