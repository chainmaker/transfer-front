import { R_MONEY } from './validator';
/* eslint-disable */
export function isArr (arr) {
  return Object.prototype.toString.call(arr) === '[object Array]' && arr.length > 0
}
export function isObj (obj) {
  return Object.prototype.toString.call(obj) === '[object Object]' && JSON.stringify(obj) !== '{}'
}
export function isStr (arr) {
  return Object.prototype.toString.call(arr) === '[object String]' && arr.length > 0
}
export function isNum (num) {
  return Object.prototype.toString.call(num) === '[object Number]'
}
export function noop () { }

// 清除为空的参数、清除字符串的前后空格
export function formatParameter (params) {
  for (let key in params) {
    if (params[key] === undefined || params[key] === null || params[key] === '') {
      delete params[key]
    } else if (typeof params[key] === 'string') {
      params[key] = params[key].replace(/(^\s*)|(\s*$)/g, '')
    }
    //  else if (isObj(params[key]) || isArr(params[key])) {
    //   params[key] = formatParameter(params[key])
    // }
  }
}

// 日期格式化
export function format (data, pat) {
  if (!data) return '';

  if (typeof data === 'string') {
    data = new Date(data.replace(/-/g, '/'));
  } else if (typeof data === 'number') {
    data = new Date(data);
  }

  var year = data.getFullYear()
  var month = data.getMonth() + 1
  var day = data.getDate()
  var hour = data.getHours()
  var minute = data.getMinutes()
  var second = data.getSeconds()

  // 两位补齐
  var autocomplete = function (int) {
    if (isNaN(int)) {
      int = 0
    }
    return int > 9 ? int : '0' + int
  }

  if (!pat) {
    pat = 'YYYY-MM-DD'
  }

  pat = pat.replace(/YYYY/g, year)
  pat = pat.replace(/MM/g, autocomplete(month))
  pat = pat.replace(/DD/g, autocomplete(day))
  pat = pat.replace(/hh/gi, autocomplete(hour))
  pat = pat.replace(/mm/g, autocomplete(minute))
  pat = pat.replace(/ss/g, autocomplete(second))

  return pat
}
export function numberFormat (number, decimals, decPoint, thousandsSep) {
  number = (number + '').replace(/[^0-9+-Ee.]/g, '')
  // eslint-disable-next-line one-var
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 2 : Math.abs(decimals),
    sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep,
    dec = (typeof decPoint === 'undefined') ? '.' : decPoint,
    s = '',
    // eslint-disable-next-line no-shadow
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec)
      return '' + Math.ceil(n * k) / k
    }

  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  var re = /(-?\d+)(\d{3})/
  while (re.test(s[0])) {
    s[0] = s[0].replace(re, '$1' + sep + '$2')
  }

  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}
/**
 * validate weight
 * @param weight
 * @returns {boolean}
 */
export function validateWeight (weight) {
  const reg = /^([1-9][0-9]{0,1}|100)$/
  return reg.test(weight)
}
/**
 * validate telephone
 * @param telephone
 * @returns {boolean}
 */
export function validateTelephone (telephone) {
  const reg1 = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
  const reg2 = /(^(0\d{2})-(\d{8})$)|(^(0\d{3})-(\d{7})$)|(^(0\d{2})-(\d{8})-(\d+)$)|(^(0\d{3})-(\d{7})-(\d+)$)/
  return reg1.test(telephone) || reg2.test(telephone)
}

export function mGetDate () {
  var date = new Date()
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var d = new Date(year, month, 0)
  return d.getDate()
}

// 应用中对金额的统一处理
const POWER = 100; // 倍率100
export function mult (num) {
  num = Number(num);
  return parseInt(num * POWER);
}
export function divi (num) {
  num = parseInt(num);
  return Number((num / POWER).toFixed(2));
}

// 获取下一个月
export function getNextMonth (date) {
  var arr = date.split('-')
  var year = arr[0] // 获取当前日期的年份
  var month = arr[1] // 获取当前日期的月份
  var day = arr[2] // 获取当前日期的日
  var days = new Date(year, month, 0)
  days = days.getDate() // 获取当前日期中的月的天数
  var year2 = year
  var month2 = parseInt(month) + 1
  if (month2 == 13) {
    year2 = parseInt(year2) + 1
    month2 = 1
  }
  var day2 = day
  var days2 = new Date(year2, month2, 0)
  days2 = days2.getDate()
  if (day2 > days2) {
    day2 = days2
  }
  if (month2 < 10) {
    month2 = '0' + month2
  }

  var t2 = year2 + '-' + month2 + '-' + day2
  return t2
}

// 根据开始时间和增加的月数,返回对应的结束时间
export function getEndTime (start, month) {
  let startTime = start.getTime() + 3600 * 24 * 1000; // 先加一天
  startTime = format(new Date(startTime), 'YYYY-MM-DD') // 转为  2020-02-02 格式
  let startArr = startTime.split('-');
  startArr[1] = parseInt(startArr[1]) + month; // 取出月份，并加上month
  if (startArr[1] > 12) {
    var str = startArr[1]
    startArr[1] = (str % 12 == 0 ? 12 : (str % 12));
    startArr[0] = parseInt(startArr[0]) + (str % 12 == 0 ? (parseInt(str / 12) - 1) : parseInt(str / 12));
  }
  let endTime = new Date(startArr.join('-')) - 3600 * 24 * 1000; // 转为时间戳减一天
  return endTime;
}

export function getCurrentWeek () {
  //起止日期数组
  var startStop = new Array();
  //获取当前时间
  var currentDate = new Date();
  //返回date是一周中的某一天
  var week = currentDate.getDay();
  //返回date是一个月中的某一天
  var month = currentDate.getDate();
  //一天的毫秒数
  var millisecond = 1000 * 60 * 60 * 24;
  //减去的天数
  var minusDay = week != 0 ? week - 1 : 6;
  //alert(minusDay);
  //本周 周一
  var monday = new Date(currentDate.getTime() - (minusDay * millisecond));
  //本周 周日
  var sunday = new Date(monday.getTime() + (6 * millisecond));
  //添加本周时间
  startStop.push(format(new Date(monday), 'YYYY-MM-DD')); //本周起始时间
  //添加本周最后一天时间
  startStop.push(format(new Date(sunday), 'YYYY-MM-DD')); //本周终止时间
  //返回
  return startStop;
};

/***
 * 获得本月的起止时间
 */
export function getCurrentMonth () {
  //起止日期数组
  var startStop = new Array();
  //获取当前时间
  var currentDate = new Date();
  //获得当前月份0-11
  var currentMonth = currentDate.getMonth();
  //获得当前年份4位年
  var currentYear = currentDate.getFullYear();
  //求出本月第一天
  var firstDay = new Date(currentYear, currentMonth, 1);


  //当为12月的时候年份需要加1
  //月份需要更新为0 也就是下一年的第一个月
  if (currentMonth == 11) {
    currentYear++;
    currentMonth = 0; //就为
  } else {
    //否则只是月份增加,以便求的下一月的第一天
    currentMonth++;
  }


  //一天的毫秒数
  var millisecond = 1000 * 60 * 60 * 24;
  //下月的第一天
  var nextMonthDayOne = new Date(currentYear, currentMonth, 1);
  //求出上月的最后一天
  var lastDay = new Date(nextMonthDayOne.getTime() - millisecond);

  //添加至数组中返回
  startStop.push(format(new Date(firstDay), 'YYYY-MM-DD'));
  startStop.push(format(new Date(lastDay), 'YYYY-MM-DD'));
  //返回
  return startStop;
};

export function getCurrentYear () {
  //起止日期数组
  var startStop = new Array();
  //获取当前时间
  var currentDate = new Date();
  //获得当前年份4位年
  var currentYear = currentDate.getFullYear();

  //本年第一天
  var currentYearFirstDate = new Date(currentYear, 0, 1);
  //本年最后一天
  var currentYearLastDate = new Date(currentYear, 11, 31);
  //添加至数组
  startStop.push(format(new Date(currentYearFirstDate), 'YYYY-MM-DD'));
  startStop.push(format(new Date(currentYearLastDate), 'YYYY-MM-DD'));
  //返回
  return startStop;
};
// 判断url是不是图片
export function isImg (url) {
  return /^(http|https)\:(.*)(jpg|png|gif|jpeg|svg)$/gi.test(url);
};
// 判断url是不是视频
export function isVideo (url) {
  return /^(http|https)\:(.*)(mp4|mov|m4v|3gp|avi|m3u8)$/gi.test(url);
};
export function isWeek () {
  var a = ["日", "一", "二", "三", "四", "五", "六"];
  var week = new Date().getDay();
  return `星期${a[week]}`
}

// 获取以当前时间为准的 时间周期
// 例如：本周、本月、本季度、本年
export function getCurrentDates (type) {
  let res = [];
  let nowDate = new Date();
  let year = nowDate.getFullYear();
  let month = nowDate.getMonth() + 1;
  let weekDay = nowDate.getDay();
  if (type === 'year') { // 本年
    res[0] = format(new Date(year + '-01-01'));
    res[1] = format(new Date(year + '-12-31'));
  } else if (type === 'quarter') { // 本季度
    let m = 10;
    let sDate;
    while (1) {
      sDate = new Date(year + '-' + m + '-01');
      if (nowDate > sDate) break;
      m -= 3;
    }
    res[0] = format(sDate);
    res[1] = format(new Date(year + '-' + (m + 2) + '-' + getMonthEnd(m + 2)));
  } else if (type === 'month') { // 本月
    res[0] = format(new Date(year + '-' + month + '-01'));
    res[1] = format(new Date(year + '-' + month + '-' + getMonthEnd(month)));
  } else if (type === 'week') { // 本周
    res[0] = format(new Date(nowDate - 24 * 3600 * 1000 * (weekDay - 1)));
    res[1] = format(new Date(nowDate.getTime() + 24 * 3600 * 1000 * (7 - weekDay)));
  }

  return res;
}

// 获取第几周
export function getWeek (date) {
  let yearFirst = date.getFullYear() + '-01-01 00:00:00';
  let yearFirstWeek = new Date(yearFirst).getDay() || 7;
  let nowWeek = ((date - new Date(yearFirst)) / 86400000 + yearFirstWeek - 1) / 7;
  return Math.floor(nowWeek) + 1;
}

export function formatTimeStr (val, moth) {
  if (val === 0) {
    return `0秒`;
  }

  let hours = parseInt(val / 3600),
    minutes = parseInt((val - hours * 3600) / 60),
    seconds = parseInt((val - hours * 3600) % 60);

  let dateHours = hours !== 0 ? `${hours}小时` : "",
    dateMinutes = minutes !== 0 ? `${minutes}分` : "",
    dateSeconds = seconds !== 0 ? `${seconds}秒` : "";
  if (!moth) {
    return `${dateHours}${dateMinutes}${dateSeconds}`;
  } else {
    return `${dateHours}${dateMinutes}`;
  }

}

export function formatMoney (num, type = 3) { //  处理数字，type: 4=万制，3=千制
  let str = String(num);
  if (R_MONEY.test(str) === false) return NaN;
  let arr = str.split('.');
  let intStr = '';
  let len = arr[0].length
  for (let i = len - 1; i >= 0; i--) {
    intStr = arr[0][i] + intStr;
    if (i !== 0 && (len - i) % type === 0) {
      intStr = ',' + intStr;
    }
  }
  arr[0] = intStr;
  return arr.join('.');
}

export function deepClone (data) {
  let res;
  if (toString.call(data) === '[object Array]') {
    res = [];
    data.forEach(item => {
      res.push(deepClone(item));
    });
  } else if (toString.call(data) === '[object Object]') {
    res = {};
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        res[key] = deepClone(data[key]);
      }
    }
  } else {
    res = data;
  }

  return res;
}

export function debounce (fn, time = 300) {
  let timer = null;
  return function () {
    timer && clearTimeout(timer);
    let arg = arguments;
    let ctx = this;
    timer = setTimeout(() => {
      fn.apply(ctx, arg);
      clearTimeout(timer);
    }, time);
  }
}
export function throttle (fn, time = 300) {
  let timer = null;
  return function () {
    if (timer === null) {
      let arg = arguments;
      let ctx = this;
      fn.apply(ctx, arg);

      timer = setTimeout(() => {
        timer = null;
      }, time);
    }
  }
}

export function launchFullscreen (element) {
  //此方法不可以在異步任務中執行，否則火狐無法全屏
  if (element.requestFullscreen) {
    element.requestFullscreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen();
  } else if (element.oRequestFullscreen) {
    element.oRequestFullscreen();
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullScreen();
  } else {
    var docHtml = document.documentElement;
    var docBody = document.body;
    var cssText = 'width:100%;height:100%;overflow:hidden;';
    docHtml.style.cssText = cssText;
    docBody.style.cssText = cssText;
    element.style.cssText = cssText + ';' + 'margin:0px;padding:0px;';
    document.IsFullScreen = true;
  }
}

export function copyText (text) {
  let textarea = document.createElement('textarea');
  textarea.display = 'none';
  textarea.value = text;
  document.body.appendChild(textarea);
  textarea.select();
  let res = document.execCommand('copy');
  document.body.removeChild(textarea);
  textarea = null;
  return res;
}
export function composeUserName (company_name, real_name) {
  if (company_name && real_name) {
    return real_name + '(' + company_name + ')'
  } else {
    return real_name || company_name
  }
}
export function downLoadFlie(res, fileName) {
  let blob = res;
  let link = document.createElement("a");
  link.href = URL.createObjectURL(
      new Blob([blob], {})
  );
  link.download = fileName;
  document.body.appendChild(link);
  link.click();
  URL.revokeObjectURL(link.href);
}