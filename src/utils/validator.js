
// 中国手机号码
export const R_MOBILE = /^1[3-9]\d{9}$/;
// 中国座机号码
export const R_PHONE = /(^(0\d{2})-(\d{8})$)|(^(0\d{3})-(\d{7})$)|(^(0\d{2})-(\d{8})-(\d+)$)|(^(0\d{3})-(\d{7})-(\d+)$)/;
// 中国公民身份证号
export const R_IDCRAD = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
// 邮箱号
export const R_EMAIL = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
// 金额（保留两位小数）
export const R_MONEY = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
// 网址校验
export const R_URL = /^(http|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;

export const validateTelephone = (rule, value, callback = function () { }) => {
  if (R_MOBILE.test(value) || R_PHONE.test(value)) {
    callback();
    return true;
  } else {
    callback(new Error('请输入正确的电话'));
    return false;
  }
};

export const validateIdCard = (rule, value, callback = function () { }) => {
  if (R_IDCRAD.test(value)) {
    callback();
    return true;
  } else {
    callback(new Error('请输入正确的身份证号'));
    return false;
  }
};

export const validateEmail = (rule, value, callback = function () { }) => {
  if (R_EMAIL.test(value)) {
    callback();
    return true;
  } else {
    callback(new Error('请输入正确的邮箱号'));
    return false;
  }
};

export const validateMoney = (rule, value, callback = function () { }) => {
  if (R_MONEY.test(value)) {
    callback();
    return true;
  } else {
    callback(new Error('请输入正确的金额'));
    return false;
  }
};

export const validlegalbizLicNum = (rule, value, callback) => {
  let Ancode; //统一社会信用代码的每一个值
  let Ancodevalue; //统一社会信用代码每一个值的权重
  let total = 0;
  let weightedfactors = [
    1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28,
  ]; //加权因子
  //不用I、O、S、V、Z
  let str = "0123456789ABCDEFGHJKLMNPQRTUWXY";
  for (let i = 0; i < value.length - 1; i++) {
    Ancode = value.substring(i, i + 1);
    Ancodevalue = str.indexOf(Ancode);
    total = total + Ancodevalue * weightedfactors[i];
    //权重与加权因子相乘之和
  }
  let logiccheckcode = 31 - (total % 31);
  if (logiccheckcode == 31) {
    logiccheckcode = 0;
  }
  let Str = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,Q,R,T,U,W,X,Y";
  let Array_Str = Str.split(",");
  logiccheckcode = Array_Str[logiccheckcode];

  let checkcode = value.substring(17, 18);
  if (logiccheckcode != checkcode) {
    return callback(new Error("校验失败，请输入正确的统一社会信用代码！"));
  }
  return callback();
};



