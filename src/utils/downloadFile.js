import { Loading } from '@element-plus/icons-vue';
import { ElNotification } from 'element-plus';
// url下载文件
export function downloadFile (data) {
  return new Promise((resolve, reject) => {
    let options = {}, file_url, file_name;
    if (typeof data === 'string') {
      options.file_url = file_url = data;
    } else {
      options.file_url = file_url = data.file_url || data.url;
      options.file_name = file_name = data.file_name || data.name;
    }
    if (!file_url) {
      ElNotification({
        title: '下载',
        message: '下载地址错误，请稍后重试',
        type: 'error',
      });
      return reject('下载地址错误，请稍后重试');
    }
    file_name = file_name || file_url.split('/').pop();
    let type = file_name ? file_name.split('.').pop() : '';
    if (!type) {
      type = file_url.split('.').pop();
      if (type) {
        file_name += '.' + type;
      }
    }
    options.file_name = file_name;
    const Notifi = ElNotification({
      title: '下载',
      message: '文件资源获取中...',
      customClass: 'rotate-loading',
      icon: Loading,
      showClose: false,
      duration: 60000
    });
    const success = () => {
      setTimeout(() => {
        Notifi.close();
        resolve();
      }, 1000);
    };
    const fail = () => {
      setTimeout(() => {
        Notifi.close();
        window.open(file_url); // 下载失败最后使用window.open
        reject();
      }, 1000);
    };

    let specialType = ['png', 'jpeg', 'jpg', 'svg', 'gif'];
    if (specialType.includes(type)) { //  跨域图片不支持直接下载
      downloadImage(options, success, fail);
    } else if (type === 'pdf') { // pdf(在chrome中)，转blob后再下载
      downloadBlob(options, success, fail);
    } else {
      console.log(file_name);
      aLinkDownload({
        href: file_url,
        fileName: file_name
      }, success);
    }
  });
}
function downloadBlob ({ file_url, file_name }, success, fail) {
  let xhr = new XMLHttpRequest();
  xhr.open('get', file_url, true);
  xhr.setRequestHeader('Content-Type', 'application/pdf');// 设置请求头
  xhr.responseType = 'blob';
  xhr.onload = function () {
    if (this.status == 200) {
      // 接受二进制文件流
      let blob = this.response;
      const blobUrl = window.URL.createObjectURL(blob);
      // 这里的文件名根据实际情况从响应头或者url里获取
      aLinkDownload({
        href: blobUrl,
        fileName: file_name
      }, success);
      window.URL.revokeObjectURL(blobUrl);
    } else {
      fail && fail('资源下载失败');
    }
  };
  xhr.send();
}
function downloadImage ({ file_url, file_name }, success, fail) { //  下载跨域图片，通过canvas将img转换为blob下载
  file_url += '?cache=false'; // 清除缓存(页面如果加载过img比如图片展示在页面了，会缓存该图片的请求头，导致 img.crossOrigin = 'Anonymous'; 设置无效)
  let type = file_name ? file_name.split('.')[1] : '';
  let imgType = {
    png: 'image/png',
    jpg: 'image/jpeg',
    jpeg: 'image/jpeg',
    gif: 'image/gif'
  };

  let canvas = document.createElement('canvas');
  let img = document.createElement('img');
  img.crossOrigin = 'Anonymous';
  img.src = file_url;
  img.onload = function () {
    canvas.width = img.width;
    canvas.height = img.height;
    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, img.width, img.height);
    canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
    canvas.toBlob((blob) => {
      aLinkDownload({
        href: window.URL.createObjectURL(blob),
        fileName: file_name
      }, success);
    }, imgType[type]);
  };
  img.onerror = fail;
}
function aLinkDownload (obj, callback) {
  const a = document.createElement('a');
  a.href = obj.href;
  a.download = obj.fileName;
  a.click();
  callback && callback();
}