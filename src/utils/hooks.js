import { ref, isRef, watch } from 'vue';

export const useTable = function (apiFunc) {
  const tableData = ref([]);
  const page_num = ref(1);
  const page_limit = ref(10);
  const loading = ref(false);
  const total = ref(0);

  function getData (params) {
    let obj = Object.assign({
      page_num: page_num.value,
      page_limit: page_limit.value
    }, params);

    loading.value = true;
    return apiFunc(obj).then(({result}) => {
      tableData.value = result.tasks || [];
      total.value =result.task_total ||0;
      return result;
    }).finally(() => {
      loading.value = false;
    });
  }
  return {
    tableData,
    page_num,
    page_limit,
    loading,
    total,
    getData
  };
};

/**
 * 
 * @keyword {RefImpl, String} 高亮关键字
 * @isDep {Boolean} 是否需要响应式 
 * @returns 返回一个字符串处理函数
 */
export const useHighLight = function (keyword, isDep = false) {
  let kw;
  let getKeyword = () => kw;
  if (isRef(keyword)) {
    if (isDep) {
      getKeyword = () => keyword.value;
    } else {
      watch(keyword, function (val) {
        kw = val;
      });
    }
  } else {
    kw = keyword;
  }
  return function highLight (text) {
    let kwStr = getKeyword();
    if (!kwStr || !text || typeof text !== 'string') return text;
    let len = text.length;
    let num = kwStr.length;
    let resStr = '';
    for (let i = 0; i < len; i++) {
      if (
        kwStr[0] === text[i] &&
        kwStr === text.substring(i, i + num)
      ) {
        resStr += '<span class="keyword">' + kwStr + '</span>';
        i += num - 1;
      } else {
        resStr += text[i];
      }
    }
    return resStr;
    // new RegExp('*')  类似这样的特殊字符会出bug，因此下面代码弃用
    // let reg = new RegExp(kwStr, 'g');
    // let repStr = '<span class="keyword">' + kwStr + '</span>'
    // return text.replace(reg, repStr);
  };
};