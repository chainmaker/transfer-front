// 需要使用createVNode和render方法
import { createVNode, render } from 'vue';
// 需要引用的组件，这个根据自己的需求来引用
import loadingProgress from './loadingProgress.vue';

// 由于render方法需要挂载到一个已经存在的节点上，我不太想在页面放置这些节点，就使用JS来生成，如果页面中已经有节点了，这里直接穿对应节点就可以了
let node = document.getElementById('my-node');
if (!node) {
  node = document.createElement('div', { id: 'my-node' });
}

// 接下来就调用方法
// 第一个参数传入一个VUE组件，第二个参数传入组件中的props
let vm = createVNode(loadingProgress, {});
// 这句话比较重要，将原本应用程序的上下文覆盖过来
// 这里用到的app就是在入口文件里通过createApp方法创建的应用程序实例
// instance.appContext = app._context

// 使用render方法渲染组件
render(vm, node);


function start () {
  // vm.component.ctx
  vm.component.proxy.start(vm.el);
}
function done () {
  vm.component.proxy.done(vm.el);
}

export default {
  start,
  done,
};
