/* eslint-disable */
import router from './index.js';
import { useUserStore,useTabBarStore } from '@/store';
import loadingProgress from '@/layout/components/loadingProgress/index.js';

const whiteList = ['/login']; // no redirect whitelist

router.beforeEach(BeforeEachFactory());

function BeforeEachFactory () {
  function beforeEach (to, from, next) {
    const tabBarStore = useTabBarStore()
    const userStore = useUserStore();
    if (whiteList.includes(to.path)) { // 在免登录白名单，直接进入
      next();
    } else {
      if (userStore.token) {
        loadingProgress.start();
        // console.log(to)
        let obj ={
          ...to.meta,
          path:to.path
        }
        tabBarStore.addTabList(obj)
        next();
      } else {
        next('/login'); // 否则全部重定向到登录页
      }
    }
  }

  return (to, from, next) => beforeEach(to, from, next);
}

router.afterEach((to, from) => {
  loadingProgress.done();
});
