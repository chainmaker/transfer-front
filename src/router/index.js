import {
  createRouter,
  createWebHashHistory
} from 'vue-router';
import Layout from '@/layout';

export const defaultRoutes = 
  [
    {
      path: '/home',
      name: 'Home',
      meta: {
        title: '首页',
        transitionIndex: 0,
        keepAlive: true,
      },
      component: () => import('@/views/home')
    },
    {
      path: '/migration',
      name: 'Migration',
      meta: {
        title: '数据迁移',
        transitionIndex: 1,
        keepAlive: false,
      },
      component: () => import('@/views/migration')
    },
    {
      path: '/migration/Amend/add',
      name: 'Add',
      meta: {
        title: '新增',
        transitionIndex: 1,
        keepAlive: false,
      },
      component: () => import('@/views/migration/components/addMig.vue'),
    },
    {
      path: '/migration/Amend',
      name: 'Amend',
      meta: {
        title: '修改',
        transitionIndex: 1,
        keepAlive: false,
      },
      component: () => import('@/views/migration/components/addMig.vue'),
    },
    {
      path: '/log',
      name: 'Log',
      meta: {
        title: '日志',
        transitionIndex: 2,
        keepAlive: false,
      },
      component: () => import('@/views/log')
    }
  ];


const router = createRouter({
  history: createWebHashHistory(),
  routes: [{
    path: '/login',
    name: 'login',
    component: () => import('@/views/login')
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: defaultRoutes
  }
  ]
});

export default router;