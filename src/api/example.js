import { request, requestCreater,requestByIdCreater } from '@/utils/axios';
/*
  用户登录
*/
export const userLogin = requestCreater('/user/userLogin', 'post');

/*
  get 获取数据
  post 添加数据
  put 修改数据
  delete 删除数据
*/
export const exampleData = request('/data/exampleData');


//查询首页迁移进度
export const get_banner = request('/tasks/banner','get');


//查询数据迁移表格
export const get_tasks_query = request('/tasks/query', 'get');

// 创建一个task
export const createTask = requestCreater('/create_task','post');
// 更新一个task
export const update_task = requestCreater('/update_task','post');

// 启动task
export const startTask = requestByIdCreater(`/start_task/`, 'get');

// 查询task日志
export const transfer_log = requestByIdCreater(`/transfer_log/`, 'get');
// 测试是否可以链接原链节点
export const test_origin_chain = requestCreater(`/test_origin_chain/{task_id}`,'get');
// 停止任务
export const stop_task = requestByIdCreater(`/stop_task/`, 'get');
// 重启任务
export const restart_task = requestByIdCreater(`/restart_task/`, 'get');
// 任务详情
export const task_info = requestByIdCreater(`/task_info/`,'get');

// 添加原链信息
export const add_origin_chain = requestCreater("/create_task/add_origin_chain", 'post', {
  headers: {
    'Content-Type': 'multipart/form-data'
  },
},{isParmas: true});
// 修改原链信息
export const update_origin_chain = requestCreater("/create_task/update_origin_chain", 'post', {
  headers: {
    'Content-Type': 'multipart/form-data'
  },
},{isParmas: true});

// 添加目标链信息
export const add_target_chain = requestCreater("/create_task/add_target_chain", 'post', {
  headers: {
    'Content-Type': 'multipart/form-data'
  },
},{isParmas: true});

// 修改目标链信息
export const update_target_chain = requestCreater("/create_task/update_target_chain", 'post', {
  headers: {
    'Content-Type': 'multipart/form-data'
  },
}, { isParmas: true });

// 下载
export const download = requestCreater("/download", 'get');