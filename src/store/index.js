import { defineStore } from 'pinia';

// 第一个参数必须是全局唯一
export const useUserStore = defineStore('user', {
  state () {
    return {
      userInfo: {},
      token: 'token',
      count: 1,
      taskID: ''
    };
  },
  getters: {
    getCount: state => state.count
  },
  actions: {
    addCount () {
      this.count++;
    },
    setUserInfo (userInfo) {
      this.userInfo = userInfo;
    },
    setToken (token) {
      this.token = token;
    },
    setTaskID (taskID) {
      this.taskID = taskID;
    }
  }
});

const tabBarMap = {
  '/home': true
};
export const useTabBarStore = defineStore('tabBar', {
  state () {
    return {
      tabBarList: [{path: '/home',title: '首页',noClose: true}]
    };
  },
  getters: {
    
  },
  actions: {
    addTabList (list){
      if ( tabBarMap[list.path]) {
        return;
      } else {
        tabBarMap[list.path] = true;
        this.tabBarList.push(list);
        // console.log(this.tabBarList);
      } 
    },
    deleTbaList (index){
      const deleItem =  this.tabBarList.splice(index,1)[0];
      tabBarMap[deleItem.path] = false;
    
    }
  }
});