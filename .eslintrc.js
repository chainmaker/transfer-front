/* eslint-disable */
module.exports = {
  "root": true,
  "env": {
    "node": true,
    "vue/setup-compiler-macros": true,
  },
  "globals": {
    "process": true,
  },
  "extends": [
    "plugin:vue/vue3-essential",
    "eslint:recommended"
  ],
  "parserOptions": {
    "parser": "babel-eslint",
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "modules": true
    }
  },
  "plugins": [
    "vue"
  ],
  "rules": {
    "arrow-parens": 0,
    "generator-star-spacing": 0,
    "no-unused-vars": [1, {
      "vars": "local",
      "args": "none"
    }],
    "no-irregular-whitespace": 0,
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "semi": 2,
    "indent": ["error", 2, {
      "SwitchCase": 1
    }],
    "space-before-function-paren": 2,
    "keyword-spacing": 2,
    "key-spacing": 2,
    "eol-last": 0,
    "no-useless-escape": "off",
    "guard-for-in": "error",
    "no-prototype-builtins": 0,
    "vue/no-multiple-template-root": 0,
    "vue/multi-word-component-names": 0,
    "vue/max-attributes-per-line": 0,
    "space-infix-ops": [1, {
      "int32Hint": true
    }]
  }
}