FROM node:14.17.4 as builder
# MAINTAINER alanhe "alanqianghe@tencent.com"

ENV NODE_ENV development

RUN echo " ------------------Web打包 --------------------"

WORKDIR /migration-web

COPY . /migration-web
RUN npm config set registry https://registry.npm.taobao.org
RUN npm install 
RUN npm run build

RUN echo " ------------------Web容器部署启动 --------------------"

FROM nginx:1.19.2
COPY --from=builder /migration-web/dist /usr/share/nginx/html
COPY deploy/nginx/conf.d /etc/nginx/conf.d
EXPOSE 80